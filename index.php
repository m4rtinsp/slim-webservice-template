<?php

// Paths
DEFINE('DS', DIRECTORY_SEPARATOR);
DEFINE('ROOT_PATH', dirname(__file__) . DS);

// Init
require ROOT_PATH . 'core' . DS . 'init.php';