<?php

class Utils
{
  public static function response($data, $status_code=200)
  {
    $app = \Slim\Slim::getInstance();

    // Http response code
    $app->response->setStatus($status_code);

    $app->response->headers->set('Content-Type', 'application/json');

    echo json_encode($data);
  }
}