<?php

date_default_timezone_set('America/Sao_Paulo');

$settings = array(
  'driver'    => getenv('DATABASE_DRIVER'),
  'host'      => getenv('DATABASE_HOST'),
  'database'  => getenv('DATABASE_NAME'),
  'username'  => getenv('DATABASE_USER'),
  'password'  => getenv('DATABASE_PASSWORD'),
  'charset'   => getenv('DATABASE_CHARSET'),
  'collation' => getenv('DATABASE_COLLATION'),
  'prefix'    => getenv('DATABASE_PREFIX')
);

/* Setup Eloquent */
use Illuminate\Database\Capsule\Manager as Capsule;
use Illuminate\Events\Dispatcher;
use Illuminate\Container\Container;

$capsule = new Capsule;
$capsule->addConnection($settings);

$capsule->setEventDispatcher(new Dispatcher(new Container));
$capsule->bootEloquent();