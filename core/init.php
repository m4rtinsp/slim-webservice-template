<?php

require ROOT_PATH . 'vendor' . DS . 'autoload.php';

// Env control
$dotenv = new Dotenv\Dotenv(dirname(__DIR__));
$dotenv->load();

// Debug
if (getenv('ENV') == 'development') {
  ini_set('display_errors', 1);
}

// Basic
require ROOT_PATH . 'core' . DS . 'utils.php';
// Config
require ROOT_PATH . 'core' . DS . 'database.php';

// Prepare App
$app  = new \Slim\Slim(array(
  'debug' => (getenv('ENV') == 'development' ? true : false),
  'mode'  => getenv('ENV')
));

$app->setName(getenv('APP_NAME'));

// Api Authentication
require ROOT_PATH . 'core' . DS . 'authenticate.php';
// Routes
require ROOT_PATH . 'app' . DS . 'routes.php';

// Run App
$app->run();