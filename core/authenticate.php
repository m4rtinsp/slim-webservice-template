<?php

function authenticate(\Slim\Route $route) {
  if (getenv('ENV') != 'development') {
    // Getting request headers
    $headers = apache_request_headers();
    $app = \Slim\Slim::getInstance();
    $response = [];

    // Verifying Authorization Header
    if (isset($headers['Authorization']) OR isset($headers['AUTHORIZATION'])) {
      // get the api key
      $api_key = (isset($headers['Authorization']) ? $headers['Authorization'] : $headers['AUTHORIZATION']);

      // validating api key
      if (!Api::isValidKey($api_key)) {
        // api key is not present in users table
        $response["error"] = "Ops. Access Denied.";
        
        Utils::response($response, 401);

        $app->stop();
      }
    }
    else {
      // api key is missing in header
      $response["error"] = "Api key is missing";
      
      Utils::response($response, 400);

      $app->stop();
    }
  }
}

// Function override
if (!function_exists('apache_request_headers')) {
  function apache_request_headers() {
    $arh = [];
    $rx_http = '/\AHTTP_/';

    foreach ($_SERVER as $key => $val) {
      if ( preg_match($rx_http, $key) ) {
        $arh_key = preg_replace($rx_http, '', $key);

        // do some nasty string manipulations to restore the original letter case
        // this should work in most cases
        $rx_matches = explode('_', $arh_key);

        if (count($rx_matches) > 0 AND strlen($arh_key) > 2) {
          foreach ($rx_matches as $ak_key => $ak_val) {
            $rx_matches[$ak_key] = ucfirst($ak_val);
          }

          $arh_key = implode('-', $rx_matches);
        }

        $arh[$arh_key] = $val;
      }
    }

    return $arh;
  }
}