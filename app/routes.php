<?php

$app->get('/', 'authenticate', function() use ($app) {
  Utils::response(App\BaseController::hello($app));
});

// Custom errors
$app->notFound(function () {
  Utils::response(App\BaseController::error());
});