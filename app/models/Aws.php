<?php

use Aws\S3\S3Client;
use Aws\S3\Enum\CannedAcl;
use Aws\S3\Exception\S3Exception;

class Aws extends BaseModel
{
  static public $key;
  static public $secret;
  static public $bucket;

  public static function s3Put($file, $object, $folder, $removeTempFile=false)
  {
    try {
      $s3 = S3Client::factory([
        'key'     => getenv('S3_KEY'),
        'secret'  => getenv('S3_SECRET'),
      ]);

      $result = $s3->putObject([
        'Bucket'  => getenv('S3_BUCKET'),
        'Key'     => $folder . '/' . $object,
        'Body'    => fopen($file, 'r'),
        'ACL'     => CannedAcl::PUBLIC_READ
      ]);

      // Remove local file
      if ($removeTempFile) {
        @unlink($file);
      }

      return [
        'status' => true,
        'result' => $result
      ];
    }
    catch(S3Exception $e) {
      return [
        'status'  => false,
        'message' => self::exceptionMessage($e)
      ];
    }
  }

  public static function s3Delete($object)
  {
    try {
      $s3 = S3Client::factory([
        'key'    => getenv('S3_KEY'),
        'secret' => getenv('S3_SECRET'),
      ]);

      $result = $s3->deleteObject([
        'Bucket'  => getenv('S3_BUCKET'),
        'Key'     => $object
      ]);

      return [
        'status' => true,
        'result' => $result
      ];
    }
    catch(S3Exception $e) {
      return [
        'status'  => false,
        'message' => $e->getMessage()
      ];
    }
  }
}