<?php

class Api extends BaseModel
{
  const ACTIVE    = 1;
  const INACTIVE  = 0;

  protected $table = 'api';

  public static function isValidKey($key)
  {
    $exists = self::whereRaw('`key` = ? AND status = ?', [$key, self::ACTIVE])->count();
    return ($exists > 0);
  }

  public static function generateKey()
  {
    return md5(uniqid(rand(), true));
  }
}