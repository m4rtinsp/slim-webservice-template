<?php

namespace App;

class BaseModel extends \Illuminate\Database\Eloquent\Model
{
  static public function exceptionMessage($e, $tryCatchOrigin=null)
  {
    $message = $e->getMessage() . ' on ' . $e->getFile() . ' in line ' . $e->getLine() .
      ($tryCatchOrigin ? ' - Try Catch: ' . $tryCatchOrigin : '');

    return $message;
  }

  static public function validate($app, $fields)
  {
    foreach ($fields as $field) {
      if (!$app->request->post($field)) {
        return false;
      }
    }

    return true;
  }
}