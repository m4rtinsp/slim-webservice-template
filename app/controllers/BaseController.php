<?php

namespace App;

class BaseController
{
  static public function hello($app)
  {
    return ['webservice' => 'On', 'project_name' => $app->getName()];
  }

  static public function error()
  {
    return ['error'=>'404'];
  }
}