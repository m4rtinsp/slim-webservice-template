# Bootstrap - Api

### Framework
* Slim (versão 2.+) 

### Desenvolvedor(es)
* **Paulo Martins** - Back-end

## Setup

* composer **install**
* composer **update**

## Tips

* After add/change name of a Model/Controller, run the command **composer dump-autoload** on terminal